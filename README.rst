==
CV
==

CV is a simple app for managing a computer scientist CV

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "cv" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'cv',
    ]

2. Include the agenda URLconf in your project urls.py like this::

    url(r'^cv/', include('cv.urls',namespace='cv')),

3. Run `python manage.py migrate` to create the publications models.

4. Start the development server and visit http://127.0.0.1:8000/cv/ to use
   the app
