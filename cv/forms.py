from django import forms
from .models import ExpType,VariousType

def get_experiences():
        exptype_list = [(idx,x.identifier) for idx,x in enumerate(ExpType.objects.all())]
        return exptype_list

def get_various():
        vartype_list = [(idx,x.identifier) for idx,x in enumerate(VariousType.objects.all())]
        return vartype_list

class DumpForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super(DumpForm, self).__init__(*args, **kwargs)
        self.fields['author'] =  forms.BooleanField(required=False)
        self.fields['experiences'] = forms.MultipleChoiceField(
            choices=get_experiences(),widget=forms.CheckboxSelectMultiple,required=False)
        self.fields['various'] = forms.MultipleChoiceField(
            choices=get_various(),widget=forms.CheckboxSelectMultiple,required=False)
