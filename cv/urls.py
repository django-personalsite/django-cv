from django.conf.urls import url

from . import views
from .models import ExpType


urlpatterns = [
    url(r'exp/(?P<type>\w+)/$', views.experience,name="experience"),
    url(r'var/(?P<type>\w+)/$', views.various,name="various"),
    url(r'dump/$',views.dumpprep,name="dump"),
    url(r'$',views.index,name="index"),
]
