from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator


# Create your models here.
try:
    LANGS=settings.LANGUAGES
except:
    LANGS=(("en","English"),)
#LANGS=(
#        ("en","English"),
#        ("fr","French"),
#        )


class ExpType(models.Model):
    identifier=models.CharField(max_length=200,unique=True)
    priority=models.IntegerField()
    public=models.BooleanField(default=False)

    def __str__(self):
        return self.identifier

    class Meta:
        verbose_name="Experience Type"

class ExpTypeTitle(models.Model):
    title=models.CharField(max_length=200)
    lang=models.CharField(max_length=10,choices=LANGS,default="en")
    exptype=models.ForeignKey(ExpType,related_name="exptypetitle")

    def __str__(self):
        return self.title

    class Meta:
        verbose_name="Experience Type Title"

class MetaExp(models.Model):
    identifier=models.CharField(max_length=200,unique=True)
    startyear=models.IntegerField()
    endyear=models.IntegerField()
    frontpage=models.BooleanField()
    priority=models.IntegerField()
    type=models.ForeignKey(ExpType)

    def __str__(self):
        return self.identifier

    class Meta:
        verbose_name="Localized Experience"

class Experience(models.Model):
    title=models.CharField(max_length=200)
    duration=models.CharField(max_length=200,blank=True)
    description=models.TextField(blank=True)
    lang=models.CharField(max_length=10,choices=LANGS,default="en")
    exp=models.ForeignKey(MetaExp,related_name="metaexp",null=True)


    def __str__(self):
        return self.title

    class Meta:
        verbose_name="Localized Experience Description"


class Place(models.Model):
    entity=models.CharField(max_length=200)
    place=models.CharField(max_length=200)
    exp=models.ForeignKey(Experience,related_name="place")

    def __str__(self):
        return self.entity+" "+self.place

class VariousType(models.Model):
    identifier=models.CharField(max_length=200,unique=True)
    public=models.BooleanField(default=False)

    def __str__(self):
        return self.identifier

    class Meta:
        verbose_name="Various Type"

class VariousTypeTitle(models.Model):
    title=models.CharField(max_length=200)
    lang=models.CharField(max_length=10,choices=LANGS,default="en")
    vartype=models.ForeignKey(VariousType)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name="Various Type Title"


class MetaVarious(models.Model):
    identifier=models.CharField(max_length=200)
    priority=models.IntegerField()
    type=models.ForeignKey(VariousType)

    def __str__(self):
        return self.identifier
    
    class Meta:
        verbose_name_plural="Localized Various"

class Various(models.Model):
    title=models.CharField(max_length=200)
    description=models.TextField(blank=True)
    lang=models.CharField(max_length=10,choices=LANGS,default="en")
    var=models.ForeignKey(MetaVarious,related_name="metavar",null=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name="Localized Various Description"

class MetaSkill(models.Model):
    identifier=models.CharField(max_length=200)
    level=models.PositiveIntegerField("Level (0-100)",validators=[MinValueValidator(0), MaxValueValidator(100)])

    def __str__(self):
        return self.identifier

    class Meta:
        verbose_name="Localized Skill"


class Skill(models.Model):
    title=models.CharField(max_length=100)
    lang=models.CharField(max_length=10,choices=LANGS,default="en")
    skill=models.ForeignKey(MetaSkill,related_name="metaskill",null=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name="Localized Skill Description"


class SingletonModel(models.Model):
    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.__class__.objects.exclude(id=self.id).delete()
        super(SingletonModel, self).save(*args, **kwargs)

    @classmethod
    def load(cls):
        try:
            return cls.objects.get()
        except cls.DoesNotExist:
            return cls()


class Author(SingletonModel):
    first_name=models.CharField(max_length=100)
    family_name=models.CharField(max_length=100)
    email=models.TextField(blank=True)
    address=models.TextField(blank=True)
    phone=models.TextField(blank=True)
    website=models.TextField(blank=True)
    other=models.TextField(blank=True)

    def __str__(self):
        return self.first_name+" "+self.family_name


    class Meta:
        verbose_name_plural="Author"

class AuthorDescription(models.Model):
    citizenship=models.CharField(max_length=100)
    description=models.TextField()
    shortdesc=models.TextField()
    lang=models.CharField(max_length=10,choices=LANGS,default="en")
    author=models.ForeignKey(Author)
