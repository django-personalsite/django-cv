from django.contrib import admin
from .models import Experience,ExpType,ExpTypeTitle,MetaExp
from .models import Place,MetaSkill
from .models import VariousType,VariousTypeTitle,Various,MetaVarious,Skill
from .models import Author,AuthorDescription
from django.conf import settings
from django.forms.models import BaseInlineFormSet
from nested_inline.admin import NestedStackedInline, NestedModelAdmin

# Register your models here.

# Utility class for all localized content
class LangFormSet(BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        super(LangFormSet, self).__init__(*args, **kwargs)
        #self.initial=[{'lang':t[0]} for t in settings.LANGUAGES ]

# Place of Experience
class PlaceInline(NestedStackedInline):
    model = Place
    extra = 1

# ExpTypeTitle
class ExpTypeTitleAdmin(admin.ModelAdmin):
    list_filter = ("lang",)

class ExpTypeTitleAdminInline(admin.StackedInline):
    formset=LangFormSet
    model = ExpTypeTitle
    max_num = len(settings.LANGUAGES)

# ExpType
class ExpTypeAdmin(admin.ModelAdmin):
    inlines = (ExpTypeTitleAdminInline, )

# Experience
class ExperienceAdminInline(NestedStackedInline):
    inlines = [PlaceInline,]
    formset=LangFormSet
    model = Experience
    max_num = len(settings.LANGUAGES)

class ExperienceAdmin(NestedModelAdmin):
    inlines = [PlaceInline,]
    list_filter = ('lang',)

# Meta - Experience
class MetaExpAdmin(NestedModelAdmin):
    inlines = [ExperienceAdminInline,]
    list_filter = ("type__identifier", )


# VariousTypeTitle
class VariousTypeTitleAdmin(admin.ModelAdmin):
    list_filter = ("lang",)

class VariousTypeTitleAdminInline(admin.TabularInline):
    formset=LangFormSet
    model = VariousTypeTitle
    max_num = len(settings.LANGUAGES)

# VariousType
class VariousTypeAdmin(admin.ModelAdmin):
    inlines = (VariousTypeTitleAdminInline, )

# Various
class VariousAdminInline(admin.StackedInline):
    formset=LangFormSet
    model = Various
    max_num = len(settings.LANGUAGES)

class VariousAdmin(admin.ModelAdmin):
    list_filter = ("lang", )

# Meta - Various
class MetaVariousAdmin(admin.ModelAdmin):
    inlines = [VariousAdminInline]
    list_filter = ("type__identifier", )

# Skill
class SkillAdmin(admin.ModelAdmin):
    list_filter = ("lang", )

class SkillAdminInline(admin.StackedInline):
    formset=LangFormSet
    model = Skill
    max_num = len(settings.LANGUAGES)


# Meta - Skill
class MetaSkillAdmin(admin.ModelAdmin):
    inlines = [SkillAdminInline]

class AuthorDescriptionAdminInline(admin.StackedInline):
    formset=LangFormSet
    model=AuthorDescription
    max_num = len(settings.LANGUAGES)

class AuthorAdmin(admin.ModelAdmin):
    inlines=(AuthorDescriptionAdminInline,)

admin.site.register(ExpType,ExpTypeAdmin)
admin.site.register(MetaExp,MetaExpAdmin)

admin.site.register(VariousType,VariousTypeAdmin)
admin.site.register(MetaVarious,MetaVariousAdmin)

admin.site.register(Author,AuthorAdmin)
admin.site.register(MetaSkill,MetaSkillAdmin)
