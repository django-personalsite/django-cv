from .models import Experience,ExpTypeTitle,Various,VariousTypeTitle

class GenLatex(object):
    header=""
    author=""
    experiences=""
    various=""

    def __init__(self,lang):
        self.lcode=lang

    def gen_header(self):
        self.header="""\\documentclass[a4paper,12pt]{cv}
    %\\usepackage[francais]{babel} 
    \\usepackage[utf8]{inputenc}  %% les accents dans le fichier.tex
    \\usepackage[T1]{fontenc}       %% Pour la césure des mots accentués
    \\usepackage[default,osfigures,scale=0.95]{opensans}
    \\usepackage[paper=a4paper,textwidth=180mm,]{geometry}

    \\newcommand{\\lieu}[1]{{#1}\\ }
    \\newcommand{\\activite}[1]{\\textbf{#1}\\ }
    \\newcommand{\\comment}[1]{\\textsl{#1}\\ }
    \\voffset -2cm 
    \\textheight 27,7cm

    \\def\\interp{2mm}
    \\def\\idp{0.55cm}
    \\def\\decrub{1cm}"""

    def gen_author(self,author):
        self.author+="""\\begin{chapeau}
          \\begin{adresse}"""

        self.author+=author.first_name+" "+author.family_name+"\\\\\n"
        self.author+=author.address.replace("\n","\\\\\n")+"\\\\\n"
        self.author+="\\ligne\\\\\n"
        self.author+="Tel : "+author.phone.replace("\n","\\\\\n")+"\\\\\n"
        self.author+="Mail : "+author.email.replace("\n","\\\\\n")+"\\\\\n"
        self.author+="Website : "+author.website.replace("\n","\\\\\n")+"\\\\\n"
        self.author+="\\end{adresse}\n"
        self.author+="\\begin{etatcivil}\n"

        try:
            desc=author.authordescription_set.get(lang=self.lcode)
            if self.lcode=="fr":
                self.author+="Nationalité "+desc.citizenship+"\\\\"
            else:
                self.author+="Citizenship : "+desc.citizenship+"\\\\"

            self.author+="\\ligne\\\\"
            self.author+=desc.shortdesc.replace("\n","\\\\\n")+"\\\\\n"
        except:
            pass
        self.author+="\\end{etatcivil}"
        self.author+="\\end{chapeau}\\vspace{0.5cm}%encadrer\n\n\n "

    def gen_exp(self,explist):
        for exp in explist:
            experience_list = Experience.objects.filter(exp__type__identifier=exp,lang=self.lcode).order_by('exp__type__identifier','-exp__endyear','exp__priority')

            if len(experience_list):
                type=exp
                typetrans=""
                try:
                    typetrans=ExpTypeTitle.objects.get(exptype__identifier=type,lang=self.lcode).title
                except:
                    typetrans=type

                self.experiences+="\\begin{rubriquetableau}[2.2cm]{"+typetrans+"}\n"

                for experience in experience_list:

                    if experience.exp.startyear != experience.exp.endyear:
                        self.experiences+=str(experience.exp.startyear)+" - "+str(experience.exp.endyear)+" &\n"
                    else:
                        self.experiences+=str(experience.exp.startyear)+" &\n"
                    self.experiences+="\\activite{"+experience.title+" - "+experience.duration +"} -- \\comment{"+experience.description.replace('\n',' ').replace('\r','')+"}\\\\\n"
                    for place in experience.place.all():
                        self.experiences+="& \hspace{\\idp}\lieu{"+place.entity+" - "+place.place+"}\\\\\n"
                    self.experiences+="\\vspace{\\interp}\\\\\n"

                self.experiences+="\\end{rubriquetableau}\n\n"


    def gen_various(self,varlist):
        for var in varlist:
            various_list = Various.objects.filter(var__type__identifier=var,lang=self.lcode).order_by('var__type__identifier','var__priority')
            if len(various_list):
                type=var
                typetrans=""
                try:
                    typetrans=VariousTypeTitle.objects.get(vartype__identifier=type,lang=self.lcode).title
                except:
                    typetrans=type

                self.various+="\\begin{rubrique}{"+typetrans+"}\n\\begin{sloppypar}\\noindent"

                for various in various_list:
                    self.various+="\\textbf{"+various.title+"}\n"+various.description.replace('\n',' ').replace('\r','')+"\\\\\n"

                self.various+="\\end{sloppypar}\\end{rubrique}\n\n"

    def gen_latex(self):
        content=""
        content+=self.header
        content+="\\begin{document}\n"
        content+=self.author
        content+=self.experiences
        content+="""\\begin{rubrique}{Publications} 
      \\begingroup
      \\renewcommand{\\section}[2]{}%
    \\nocite{*}
    \\bibliographystyle{unsrt}
    """
        content+="\\bibliography{CV_"+self.lcode+"}"
        content+="""\\endgroup
    \\end{rubrique}
    """
        content+=self.various
        content+="\\end{document}\n"

        return content
