from django.shortcuts import render
from .models import Experience,ExpTypeTitle,ExpType,MetaExp
from .models import Various,VariousTypeTitle,VariousType
from .models import MetaSkill,Skill,Author
from django.utils import translation
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from .forms import DumpForm
import cv.genlatex

def index(request):
     lcode=translation.get_language()
     exp_types = ExpTypeTitle.objects.filter(lang=lcode).order_by('exptype__priority')
     context={}
     dictarr={}
     counter=0
     for exptype in exp_types:
         experiences = Experience.objects.filter(exp__frontpage=True,exp__type=exptype.exptype,lang=lcode).order_by('-exp__endyear','exp__priority')
         if len(experiences):
             counter+=1
                
             dictarr[exptype]=experiences

     context["dictarr"]=dictarr
     
     skills= Skill.objects.filter(lang=lcode).order_by('-skill__level')

     context["skills"]=skills

     try:
         author=Author.objects.get()
         context["author"]=author
     except:
         pass
     return render(request, 'cv/index.html', context)


# Create your views here.
def experience(request,type=None):
     lcode=translation.get_language()
     exptype=get_object_or_404(ExpType,identifier=type)
     if exptype.public or request.user.is_authenticated():
         experience_list = Experience.objects.filter(lang=lcode,exp__type__identifier=type).order_by('-exp__endyear','exp__priority')
         exptypes=ExpTypeTitle.objects.filter(exptype__identifier=type)
         exptitle=""
         try:
             exptitle=exptypes.get(lang=lcode)
         except:
             pass


         context = {
            'title': exptitle,
            'experience_list' : experience_list,
         }
         return render(request, 'cv/experience.html', context)
     else:
        return  HttpResponse("Only authenticated user can do that")
 
def various(request,type=None):
     lcode=translation.get_language()
     vartype=get_object_or_404(VariousType,identifier=type)
     if vartype.public or request.user.is_authenticated():
         various_list = Various.objects.filter(lang=lcode,var__type__identifier=type).order_by('var__priority')
         vartypes=VariousTypeTitle.objects.filter(vartype__identifier=type)
         vartitle=""
         try:
             vartitle=vartypes.get(lang=lcode)
         except:
             pass

         context = {
            'title': vartitle,
            'various_list' : various_list,
         }
         return render(request, 'cv/various.html', context)
     else:
        return  HttpResponse("Only authenticated user can do that")

def dumpprep(request):
    if request.user.is_authenticated():
        lcode=translation.get_language()
        if request.method == 'POST':
        # create a form instance and populate it with data from the request:
            form = DumpForm(request.POST)
            if form.is_valid():
                latex=cv.genlatex.GenLatex(lcode)
                latex.gen_header()
                if form.cleaned_data['author']:
                    try:
                        author=Author.objects.get()
                    except:
                        pass
                    latex.gen_author(author)
                experiences=[form.fields['experiences'].choices[int(idx)][1] for idx in form.cleaned_data['experiences']]
                latex.gen_exp(experiences)
                various=[form.fields['various'].choices[int(idx)][1] for idx in form.cleaned_data['various']]
                latex.gen_various(various)

                return HttpResponse(latex.gen_latex(),content_type="text/plain;charset=utf-8")
            else:
                return HttpResponse("Error")
        else:
            form=DumpForm()
            context = { 'form':form,} 
            return render(request,'cv/dump.html',context)
    else:
        return  HttpResponse("Only authenticated user can do that")


